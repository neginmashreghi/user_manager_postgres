const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const UserSchema = new Schema({
  username: String,
  email: String,
  password: String,
}, {timestamps: true});

module.exports = User = mongoose.model("users", UserSchema);