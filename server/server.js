const express = require("express");
const bodyParser = require('body-parser')
const mongoose = require("mongoose");
const usersRouter = require("./routers/user");
const cors = require('cors');
const client = require('./database/dbConfig')
const app = express()
var http = require('http').createServer(app);
var io = require('socket.io')(http);
const port = process.env.PORT || "8000";


// Database config and connect
client.connect(err => {
  if (err) {
    console.error('connection error', err.stack)
  } else {
    console.log('connected to database')
  }
})

const listener = client.query("LISTEN watchers");
// When ever the event is triggered, PSQL is gonna send a notification, so in order to do stuff with it let’s listen to it.
client.on('notification', async (data)=>{
  const payload = JSON.parse(data.payload)
  if(payload.table === "users"){
    io.emit('renderTableComponenet'); 
  }
});


//  App Configuration
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(cors());


// Routes Definitions
app.use("/api/users", usersRouter);


// Server Activation
http.listen(port, () => {
    console.log(`Listening to requests on http://localhost:${port}`);
});