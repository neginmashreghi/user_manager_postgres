const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");
//const UserModal = require("../modals/User");
const db = require("../database/dbConfig")

router.get("/test", (req, res) => res.json({ msg: "Users Router Works" }));

router.get("/all", async (req, res) => {
    const AllUserQuery =`SELECT * FROM users `
  const { rows } = await db.query(AllUserQuery)
  if (rows.length !== 0){
    console.log("get all the users" ) 
    res.json(rows) 
    }else {
        return res.status(400).json({ email: "Not able to get all rows" });
    }
  
});

router.delete('/delete', async (req, res, next) => {
 
    const deleteUserQuery =`DELETE FROM users WHERE email='${req.body.email}' RETURNING *;`
   
    const { rows } = await db.query(deleteUserQuery)
    if (rows.length !== 0){
        res.json(rows)  
    }else {
        return res.status(400).json({ email: "Not able to delete a row" });
    }
    
});

router.put('/update', async (req, res, next) => {
    
    const UpdateQuery =`UPDATE users SET username ='${req.body.username}', email ='${req.body.email}' WHERE  employeeId= '${req.body.id}'  ;`
    const { rows } = await db.query(UpdateQuery)
    if (rows.length !== 0){
        console.log("update user info in database" ) 
        res.json({ success: "update user info in database" });    
    }else {
        return res.status(400).json({ email: "Not able to update a row" });
    }

     
    
});

router.post("/add", async (req, res) => {

  
    const findUserEmailQuery =`SELECT username  From users  WHERE email ='${req.body.email}';`
    const { rows } = await db.query(findUserEmailQuery)

    if (rows.length !== 0){
        return res.status(400).json({ email: "Email already exists" });
    }else {
        insertQuery =`INSERT INTO users (username, email, user_password) VALUES('${req.body.username}','${req.body.email}', crypt('${req.body.password}', gen_salt('bf')) ) RETURNING employeeId ; `
        const {rows} = await db.query(insertQuery)
        console.log("new user add to database" )
        res.json({ success: "new user add to database" });
    } 

});


module.exports = router;