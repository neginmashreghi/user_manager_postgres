CREATE EXTENSION pgcrypto;
CREATE TABLE users (
    employeeId SERIAL PRIMARY KEY,
    username VARCHAR(45) NOT NULL,
    email VARCHAR(100) NOT NULL UNIQUE,
    user_password TEXT NOT NULL,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

INSERT INTO users (username, email, user_password)
VALUES('admin', 'admin@gmail.com', crypt('123456', gen_salt('bf')));

CREATE OR REPLACE FUNCTION notify_trigger() 
RETURNS trigger AS $$ 
BEGIN 
PERFORM pg_notify( 'watchers', json_build_object( 'table', TG_TABLE_NAME ,'operation', TG_OP, 'record', row_to_json(NEW) )::text ); 
RETURN NEW; 
END; 
$$ LANGUAGE plpgsql; 

CREATE TRIGGER users_changed 
AFTER INSERT OR UPDATE
ON users
FOR EACH ROW 
EXECUTE PROCEDURE notify_trigger();

CREATE TRIGGER users_changed_delete
BEFORE DELETE
ON users
FOR EACH ROW 
EXECUTE PROCEDURE notify_trigger();