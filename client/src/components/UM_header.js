import React, { Component } from 'react'
import { Button  } from 'antd';
import { PlusOutlined} from '@ant-design/icons';
import FormModal from "./UM_modal"

export default class UM_header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title:"",
            visible :false,
            formType: ""
        };
    }

    handelAddUser = () => {
        this.setState(
         {
            title:"Add User",
            visible:true,
            formType: "add"
        })

    }

    closeModal=()=>{
        // all user
        this.setState({visible:false})
    }
    
    render() {
        return (
            <div className="user-manager-header-wrapper">
                <h1>User Master</h1>
                <div>
                    <Button type="primary" shape="round" icon={<PlusOutlined />} size={"large"} onClick={this.handelAddUser}>
                       Add User
                    </Button>
                   
                </div>

              <FormModal
                    title={this.state.title}
                    visible={this.state.visible}
                    onCancel={this.closeModal}
                    formType={this.state.formType}
                /> 
           </div>
        )
    }
}
