import React, { Component } from 'react'
import { Button , Table, Popconfirm,message } from 'antd';
import { DeleteOutlined , EditOutlined} from '@ant-design/icons';
import io from "socket.io-client";
import FormModal from "./UM_modal"
import  axios from 'axios';
import { connect } from "react-redux"; 
import {getAllUsers, deleteUser} from '../redux/actions/usersAction'
const socket = io("http://localhost:8000/");

const mapStateToProps = state => {
    const usersState = state.users
    return { 
        allUsers: usersState.users,
        respond: usersState.respond,
        errors:usersState.error
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getAllUsers: () => dispatch(getAllUsers()),
        deleteUser: (id) => dispatch(deleteUser(id)),
    }
}

 class UM_table extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataSource:[],
            title:"",
            visible :false,
            formType: "",
            username:"",
            email:"",
            role:"",
            id:""
        };
    }

    componentDidMount(){
        socket.on("renderTableComponenet", ()=>
        {
            this.props.getAllUsers()
        });
        this.props.getAllUsers()
  
    }

    componentDidUpdate(prevProps, prevStat){
          if (prevProps.allUsers !== this.props.allUsers) {
               this.setState({ dataSource : this.props.allUsers});     
          } 
    } 
   
    handleDelete =  email => {  
        console.log(email)
        const dataSource = [...this.state.dataSource];
        this.props.deleteUser(email)
        message.success('user had been deleted');
        this.setState({
            dataSource: dataSource.filter(item => item.email !== email),
        });
    };
  
    // open the modale and pass the record data to the modal from
    handelUpdate =(record)=>{
        
          this.setState(
          {
              title:"Update User",
              visible:true,
              formType: "update",
              username: record.username,
              email: record.email,
              id:record.employeeid
          })
  
    }
  
    closeModal=()=>{
          // all user
          this.setState({visible:false})  
    }

    render() {
console.log(this.state.dataSource)
        const columns = [
           
            {
                title: 'User Id',
                dataIndex: 'employeeid',
                key: 'employeeid',
            },
            {
                title: 'User Name',
                dataIndex: 'username',
                key: 'username',
            },
            {
                title: 'Email',
                dataIndex: 'email',
                key: 'email',
            },
            {
                title: 'Created At',
                dataIndex: 'created_at',
                key: 'created_at',
                render: text => {
                    const d = new Date(text)
                    return(d.toLocaleString())
                  },
                  
            },
    
            {
                title: 'Operation',
                dataIndex: 'operation',
                render: (text, record) =>
                  this.state.dataSource.length >= 1 ? (
                      <div>   
                           <Button style={{marginRight:"10px"}} type="primary" shape="circle" icon={<EditOutlined />} onClick={() => this.handelUpdate(record)} />         
                            <Popconfirm title="Sure to delete?" onConfirm={() => this.handleDelete(record.email)}>
                            <Button type="primary" shape="circle" icon={<DeleteOutlined />} />
                            </Popconfirm>
                   
                    </div>   
                  ) : null,
              },
            ];
        return (
            <div>
                <Table 
                className="user-manager-table-wrapper"
                dataSource={this.state.dataSource} 
                columns={columns} 
                bordered
                scroll={{ y: 500 }}
                pagination={false}
                />
                <FormModal
                title={this.state.title}
                visible={this.state.visible}
                onCancel={this.closeModal}
                formType={this.state.formType}
                username ={this.state.username}
                email={this.state.email}
                id={this.state.id}
                /> 
            </div>
        )
    }
}

const TableWrapper = connect(mapStateToProps,mapDispatchToProps)(UM_table);
export default TableWrapper;

