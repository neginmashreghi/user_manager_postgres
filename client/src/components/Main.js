import React, { Component } from 'react'
import { Layout, Menu,  } from 'antd';
import UserManager from "./UserManager"

const { Header, Content, Footer } = Layout;

export default class Main extends Component {
    render() {
        return (
            <Layout className="layout" style={{ minHeight: '100vh' }}>
                <Header>
                <div className="logo" />
                <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['1']}>
                    <Menu.Item key="1">User Manager</Menu.Item>
                </Menu>
                </Header>
                <Content style={{ padding: '0 50px' }}>
                
                <UserManager/>
                </Content>
                <Footer style={{ textAlign: 'center' }}>Created by Negin Mashreghi</Footer>
            </Layout>
        )
    }
}
