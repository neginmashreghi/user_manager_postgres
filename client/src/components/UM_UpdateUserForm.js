import React, { useState , useEffect } from 'react'
import { Form, Input, Button, Select , message } from 'antd';
import { useDispatch, useSelector } from "react-redux";
import {updateUser} from "../redux/actions/usersAction" 
import  axios from 'axios';

const { Option } = Select;


function UM_UpdateUserForm(props) {
  
    const dispatch = useDispatch();

    useEffect(() => {
       if(props.modalclose === true){
        form.resetFields();
        }
  
    }, [props.modalclose ]); 

    const [form] = Form.useForm();

    const onFinish =(value)=>{
      
        value["id"]=props.id
        console.log(value)
        dispatch(updateUser(value))
        message.success("user updates successfully")

    }

   

    return (
        <Form
            className="user-manager-form"
            onFinish={onFinish}
            form={form}
            initialValues = {{
                username:props.username,
                email:props.email,
            }}
        >
            <Form.Item
                className="form-item"
                label="Username"
                name="username"
                rules={[{ required: true, message: 'Please input username!' }]}  
                
            >
                <Input className="input" />
            </Form.Item>

            <Form.Item
                className="form-item"
                label="Email"
                name="email"
                 rules={[{ required: true, message: 'Please input email!' }]} 
            >
                <Input className="input"/>
            </Form.Item>

            <Form.Item >
                <Button type="primary" className="button" htmlType="submit">
                Update User
                </Button>
            </Form.Item>

              
        </Form>
    )
}

export default UM_UpdateUserForm
