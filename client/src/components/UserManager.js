import React, { Component } from 'react'
import Header from "./UM_header"
import Table from './UM_table'
import "./userManager.css"

export default class UserManager extends Component {
    render() {
        return (
            <div className="user-master-main-wrapper">
                <Header/>
                <Table/>
            </div>
        )
    }
}
