import { 
    ADD_USERS_SUCCESS_RESPOND, 
    ADD_USERS_ERROR_RESPOND,
    GET_ALL_USERS_SUCCESS_RESPOND,
    GET_ALL_USERS_ERROR_RESPOND,
    DELETE_USERS_SUCCESS_RESPOND,
    DELETE_USERS_ERROR_RESPOND,
    UPDATE_USERS_SUCCESS_RESPOND,
    UPDATE_USERS_ERROR_RESPOND,
} from "../types";

import axios from "axios";

export const addUser = (newUser) => dispatch => {
    axios
      .post(`http://localhost:8000/api/users/add`, newUser)
      .then(res => {
        dispatch({
            type: ADD_USERS_SUCCESS_RESPOND,
            payload: "new user is add to database"
          })
      })
      .catch(err =>{
        dispatch({
          type: ADD_USERS_ERROR_RESPOND,
          payload: err.response.data
        })
      });
};

export const getAllUsers = () => dispatch => {
   
    axios
      .get(`http://localhost:8000/api/users/all`)
      .then(res => {
        dispatch({
            type: GET_ALL_USERS_SUCCESS_RESPOND,
            payload: res.data
          })
      })
      .catch(err =>{
        dispatch({
          type: GET_ALL_USERS_ERROR_RESPOND,
          payload: err.response.data
        })
      });
};

export const deleteUser = (email) => dispatch => {

    axios
      .delete(`http://localhost:8000/api/users/delete`, {data: {
        email: email
      }})
      .then(res => {
        dispatch({
            type: DELETE_USERS_SUCCESS_RESPOND,
            payload: "user has been deleted successfully"
          })
      })
      .catch(err =>{
        dispatch({
          type: DELETE_USERS_ERROR_RESPOND,
          payload: err.response.data
        })
      });
};

export const updateUser = (updatUser) => dispatch => {
   
    axios
      .put(`http://localhost:8000/api/users/update`, updatUser)
      .then(res => {
        dispatch({
            type: UPDATE_USERS_SUCCESS_RESPOND,
            payload: "user has been Updated successfully"
          })
      })
      .catch(err =>{
       dispatch({
          type: UPDATE_USERS_ERROR_RESPOND,
          payload: err.response.data
        })
      });
  };

