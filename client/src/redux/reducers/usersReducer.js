import { 
  ADD_USERS_SUCCESS_RESPOND, 
  ADD_USERS_ERROR_RESPOND,
  GET_ALL_USERS_SUCCESS_RESPOND,
  GET_ALL_USERS_ERROR_RESPOND,
  DELETE_USERS_SUCCESS_RESPOND,
  DELETE_USERS_ERROR_RESPOND,
  UPDATE_USERS_SUCCESS_RESPOND,
  UPDATE_USERS_ERROR_RESPOND,
} from "../types";
  
const initialState = {
    users: [],
    respond:"",
    errors: ""
};
  

  
export default function (state = initialState, action) {
    switch (action.type) {
      case ADD_USERS_SUCCESS_RESPOND:
        return {
          ...state,
          respond: action.payload
        };
        case GET_ALL_USERS_SUCCESS_RESPOND:
        return {
          ...state,
          users: action.payload
        };
        case DELETE_USERS_SUCCESS_RESPOND:
            return {
              ...state,
              respond: action.payload
            };
        case UPDATE_USERS_SUCCESS_RESPOND:
          return {
            ...state,
            respond: action.payload
          };
        case ADD_USERS_ERROR_RESPOND:
          return {
            ...state,
            errors: action.payload
          };
        case GET_ALL_USERS_ERROR_RESPOND:
          return {
            ...state,
            errors: action.payload
          };
        case DELETE_USERS_ERROR_RESPOND:
          return {
            ...state,
            errors: action.payload
          };
        case UPDATE_USERS_ERROR_RESPOND:
          return {
            ...state,
            errors: action.payload
          };
      
      default:
        return state;
    }
  }