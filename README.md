
## About The APP
This application add, update, delete, and display users.


### Built With

Server is build on a Node/Express/React stack

- [NodeJS](https://nodejs.org/)
- [Express](https://expressjs.com/)
- [React](https://reactjs.org/)
- [Postgres](https://www.postgresqltutorial.com/what-is-postgresql/)
- [SocketIo](https://socket.io/)

## Getting Started

To get a hot-loaded local instance up and running. The client folder contains a CRA (create-react-app) flavoured webpack setup. For development mode, the node package [concurrently](https://www.npmjs.com/package/concurrently) is used to run a separate instance of Express and CRA. 

### Prerequisites

You will need NodeJS/npm version 10+

```sh
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.32.1/install.sh | bash
nvm install 12.x.x
npm install npm@latest -g
```
### Implement postgres
Create a Directory to Serve as the Local Host Mount Point for Postgres Data Files
```sh
mkdir -p $HOME/docker/volumes/postgres
```

Run the Postgres Container
```sh
docker run --rm   --name pg-docker -e POSTGRES_PASSWORD=docker -d -p 5432:5432 -v $HOME/docker/volumes/postgres:/var/lib/postgresql/data  postgres
```
Launch a Bash terminal within a postgres container.
```sh
docker exec -it <containerid> bash
```

Accessing a Database by running :
```sh
psql -U  postgres
```
Execute SQL commands that located in:

[SQL Queries](./server/database/sqlQueries.sql)



### Installation
Open terminal make sure you are in **user_manager_postgres** folder then start installing the packages by run these commands:

```sh
npm install
npm run server-install
npm run client-install


```

### RUN
Run client

```sh
npm run client

```
Run server 

```sh
npm run server

```

Run server and client 

```sh
npm run dev

```




